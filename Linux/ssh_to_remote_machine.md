# Conectarse por ssh a un servidor en la nube

## Copiar la clave privada del servidor Azure

Hay que copiar al ordenador local el fichero con la clave.

### En una máquina Linux (o Windows Linux subsystem), usando un terminal

Copiar la clave privada al directorio `.ssh`

Cambiarle los permisos usando `$ sudo chmod 600 ~/.ssh/prod_key`.

Los permisos quedarán así:

```
~/.ssh$ ls -l
-rw------- 1 username username 2494 Jun 30 12:39 prod_key
```

### En una máquina Windows

Se puede usar Putty o Mobaxterm (por ejemplo).

En ambos casos, hay que especificar la ubicación del fichero con la clave privada.

#### Putty

Hace falta añadir un fichero `.ppk` para que funcione. Se puede convertir la original a una `.ppk` usando [puttygen](https://aws.amazon.com/es/premiumsupport/knowledge-center/ec2-ppk-pem-conversion/).


#### Mobaxterm

Hay que indicarle la ubicación de la clave en `use private key`:



## Conectarse por SSH

En una máquina Linux:

```
$ ssh -i .ssh/prod_key Circe@x.y.z.t -X
```

La opción `-X` permite abrir en local ventanas remotas.

Una vez dentro del servidor, con `$ firefox &`, se puede abrir una ventana del navegador.


## Añadir los nombres de las conexiones

Crear un fichero local llamado `.ssh/config`, con este contenido:

```
$ cat .ssh/config
# Máquina Ateanea de desarrollo
Host atenea-development
        Hostname x.y.z.t
        User AteneaUser22
        Port 22
        IdentityFile ~/.ssh/dev_key
        IdentitiesOnly yes

# Máquina Ateanea de produccion
Host atenea-production
        Hostname x.y.z.tt
        User Circe
        Port 22
        IdentityFile ~/.ssh/prod_key
        IdentitiesOnly yes
```

De esa forma, basta con especificar el nombre para conectarse:

```
$ ssh atenea-production
```

o bien:
```
$ ssh atenea-development
```

## Copy a file to the Azure server

```
$ scp -i .ssh/prod_key embedded-sw.bin Circe@2x.y.z.t:
embedded-sw.bin                                                                                                                                      100%   88KB 871.7KB/s   00:00
```
