# Router between two computers

Create a bridge between two computers using a third (Linux) computer with two interfaces.

It can be useful e.g., for adding network delays with `netem` in PC2.

```
+-------+            +-------+            +-------+
|  PC1  |------------|  PC2  |------------|  PC3  |
|       |eth0    eth0|       |eth1    eth0|       |
+-------+            +-------+            +-------+
          192.168.1.0          192.168.2.0
```

Let’s assume the following IP addresses for your computers:
- Computer 1 (Left): `192.168.1.2`
- Computer 2 (Middle): `192.168.1.1` (for Computer 1) and `192.168.2.1` (for Computer 3)
- Computer 3 (Right): `192.168.2.2`

You can configure the network interfaces on each computer using the `ifconfig` command. Here’s how you can do it:

## On PC1
```
sudo ifconfig eth0 192.168.1.2 netmask 255.255.255.0 up
route add default gw 192.168.1.1 eth0
```

## On PC2
```
# For Computer 1
sudo ifconfig eth0 192.168.1.1 netmask 255.255.255.0 up

# For Computer 3
sudo ifconfig eth1 192.168.2.1 netmask 255.255.255.0 up

# Enable IP forwarding
echo 1 > /proc/sys/net/ipv4/ip_forward
```

## On PC3
```
sudo ifconfig eth0 192.168.2.2 netmask 255.255.255.0 up
route add default gw 192.168.2.1 eth0
```

This will set up Computer 2 as a router between Computer 1 and Computer 3. Please replace `eth0` and `eth1` with your actual network interface names. You can find them using the `ifconfig` command without any arguments.

Please note that these changes are not persistent across reboots. To make them permanent, you would need to edit the network configuration files, which depends on your specific Linux distribution. Also, this is a very basic setup and might need to be adjusted based on your specific requirements, such as needing to configure DNS, etc.