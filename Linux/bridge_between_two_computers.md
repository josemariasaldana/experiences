# Create a bridge between two computers

Create a bridge between two computers using a third (Linux) computer with two interfaces.

It can be useful e.g., for adding network delays with `netem` in PC2.


```
+-------+            +-------+            +-------+
|  PC1  |------------|  PC2  |------------|  PC3  |
|       |eth0    eth0|       |eth1    eth0|       |
+-------+            +-------+            +-------+
```

To connect two computers (PC1 and PC3), I want to use a third one (PC2) that acts as a network bridge (not a router).

## On PC2

I will use the `brctl` command to create a network bridge and add interfaces to it.

First, install the `bridge-utils` package which provides the `brctl` command. On a Debian-based system like Ubuntu, you can do this with:

```bash
sudo apt-get update
sudo apt-get install bridge-utils
```
Then, you can create a bridge and add your interfaces to it. Assuming your interfaces are named `eth0` and `eth1`, you can do:

```bash
# Create a bridge named br0
sudo brctl addbr br0

# Add interfaces to the bridge
sudo brctl addif br0 eth0
sudo brctl addif br0 eth1

# Bring up the bridge
sudo ifconfig br0 up
```

Now, `br0` acts as a single interface that is a bridge between `eth0` and `eth1` (replace `eth0` and `eth1` with your actual network interface names). You can assign an IP address to it just like any other interface using `ifconfig`:
```
sudo ifconfig br0 192.168.1.1 netmask 255.255.255.0 up
```

## On PC1 and PC3

The other two computers (Computer 1 and Computer 3) can be in the same subnet.

On Computer 1:
```
sudo ifconfig eth0 192.168.1.2 netmask 255.255.255.0 up
```

On Computer 3:
```
sudo ifconfig eth0 192.168.1.3 netmask 255.255.255.0 up
```

This way, all three computers are in the same IP subnet, and Computer 2 acts as a bridge, not a router.

Please note that these changes are not persistent across reboots. To make them permanent, you would need to edit the network configuration files, which depends on your specific Linux distribution. Also, this is a very basic setup and might need to be adjusted based on your specific requirements, such as needing to configure DNS, etc.