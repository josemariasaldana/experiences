# Embed images in Markdown

Add an image this way, so it will be its own size:

```
![embebido](images/20220721_111037.jpg)
```

Add an image with a fixed width:

```
<img src="images/luminaire/IMG_20240118_103129.jpg" alt="drawing" width="200"/>
```