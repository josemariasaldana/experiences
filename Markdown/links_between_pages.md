# Links between pages of the same repo

In that case, do not use the whole URL, but only the last part.

For example, use `Markdown/collapsible_secion.md`.