# Add a collapsible section in markdown

Taken from https://gist.github.com/pierrejoubert73/902cc94d79424356a8d20be2b382e1ab

Use `<details>` and `</details>`.

To make it open by default, use `<details open>`.

To make it closed by default, use `<details close>`.

Example:

```
<details close>
<summary>Fichero producción</summary>

This section is expandable.

This is another paragraph.

And this is another paragraph.

</details>
```

This is the result:

<details close>
<summary>Fichero producción</summary>

This section is expandable.

This is another paragraph.

And this is another paragraph.

</details>