# Add a table of content in Markdown

Use this:

```
[[_TOC_]]
```

Example:

[[_TOC_]]

## Example title 1

### Example subtitle 1.1

## Example title 2